<?php

return  [
    'account/login' => [
        'controller' => 'account',
        'action'=>'login',
    ],

    'news/show' => [
        'controller'=>'news',
        'action'=>'show',
    ],
    ''=>[
    	'controller'=>'main',
    	'action'=>'index',
    ],
    'account/register' => [
        'controller' => 'account',
        'action'=>'register',
    ],
];
