<?php


namespace app\core;
use app\core\view;

abstract class Controller
{
    public $route;
    public $view;
    public  function __construct($route)
    {
        $this->route = $route;
        $this->view = new view($this->route);


    }

}