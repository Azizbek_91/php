<?php
namespace app\core;
use app\core\View;
class Router
{
    protected $routes = [];
    protected $params = [];

    function __construct()
    {
        $arr = require 'app/config/routes.php';
        foreach ($arr as $key =>$val){
            $this->add($key,$val);
        }

    }
    public function add($route,$params)
    {
        $route = '#^'.$route.'$#';
        $this->routes[$route] = $params;

    }

    public function match()
    {
        $url = trim( $_SERVER['REQUEST_URI'],'/');
        foreach ($this->routes as $route => $val){
            if(preg_match($route,$url,$match)){
                $this->params = $val;
                return true;
            }
        }
        return false;
    }

    public function run()
    {
        if($this->match()){
            $path =  'app\controllers\\'.ucfirst($this->params['controller']).'Controller';
            if(class_exists($path)){
                $action = $this->params['action'].'Action';
                if(method_exists($path, $action)){
                    $controller = new $path($this->params);
                    $controller->$action();
                }else{
                    echo $action.' metodi topilmadi!';
                }
            }else{
                View::errorCode(404);

            }
        }else{
           echo trim( $_SERVER['REQUEST_URI'],'/').' route topilmadi';
        }

    }



}