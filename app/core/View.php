<?php


namespace app\core;

class view
{
    public $path;
    public $layout = 'default';
    public $route;

    public function __construct($route)
    {
        $this->route = $route;
        $this->path = $route['controller'].'/'.$route['action'];

    }

    public function render($title,$vars=[]){
        extract($vars);
        ob_start();
        require 'app/views/'.$this->path.'.php';
        $content = ob_get_clean();
        require 'app/views/layouts/'.$this->layout.'.php';
    }

    public function errorCode($code){
        http_response_code($code);
        require 'app/views/erorrs/'.$code.'.php';
        exit;
    }


}